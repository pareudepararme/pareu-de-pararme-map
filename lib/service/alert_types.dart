

/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:pareu_de_pararme_map/models/alert_types.dart';
import 'package:pareu_de_pararme_map/service/base_api.dart';


const GET_ALERT_TYPES = '/api/getalertstypes';

Future<List<AlertType>> getAlertTypes () async {
  var alertTypesList = (await apiCall(GET_ALERT_TYPES,))["res"];
  return alertTypesList.isEmpty ? [] : [
    for (var res in alertTypesList)
      AlertType.fromJson(res)
  ];
}