/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pareu_de_pararme_map/AppBuilder.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/models/alert_types.dart';
import 'package:pareu_de_pararme_map/models/panel.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/non_foss_firebase.dart';
import 'package:pareu_de_pararme_map/ui/routes.dart';
import 'package:provider/provider.dart';
import 'generated/l10n.dart';

void main() {
  runApp(MyApp(),);
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return FireBaseInitialization(
      child: AppBuilder(
          builder: (context) {
            return MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (context) => UserAuthProvider()),
                // todo: move all below to map_screen.dart?
                ChangeNotifierProvider(create: (context) => PanelModel()),
                ChangeNotifierProvider(create: (context) => AlertsListModel()),
                ChangeNotifierProvider(create: (context) => AlertTypesProvider()),
              ],
              child: MaterialApp(
                  localizationsDelegates: [
                    S.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                  ],
                  supportedLocales: S.delegate.supportedLocales,
                  title: 'Pareu de Parar-me',
                  theme: ThemeData(
                    primarySwatch: Colors.blue,
                    // primaryColor: Colors.black,
                    // textTheme: const TextTheme(
                    //   title: TextStyle(color: Colors.white),
                    // ),
                  ),
                  initialRoute: '/',
                  onGenerateRoute: CustomRouter.generateRoute
              ),
            );
          }
      ),
    );
  }
}