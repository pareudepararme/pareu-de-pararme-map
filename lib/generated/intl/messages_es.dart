// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "addAlert": MessageLookupByLibrary.simpleMessage("Añadir alerta"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancela"),
        "captchaError": MessageLookupByLibrary.simpleMessage(
            "Error resolviendo el captcha"),
        "formAuthError": MessageLookupByLibrary.simpleMessage(
            "Error de autentificación, reintentar"),
        "formChangeAlertDate": MessageLookupByLibrary.simpleMessage(
            "Seleccione fecha de la alerta"),
        "formChangeAlertHour": MessageLookupByLibrary.simpleMessage(
            "Seleccione la hora de la alerta"),
        "formChangeDate": MessageLookupByLibrary.simpleMessage("Cambiar fecha"),
        "formConnectionError":
            MessageLookupByLibrary.simpleMessage("Error de conexión"),
        "formControlTypeLabel":
            MessageLookupByLibrary.simpleMessage("Tipo de control"),
        "formDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Por favor, añada una descripción de al menos 5 letras"),
        "formDescriptionLabel":
            MessageLookupByLibrary.simpleMessage("Descripción"),
        "formFormalizeComplaintQuestion": MessageLookupByLibrary.simpleMessage(
            "Quiere formalizar la parada?"),
        "formPointNotSelectedError": MessageLookupByLibrary.simpleMessage(
            "Seleccione un punto en el mapa para enviar la alerta"),
        "formPointOutsideBounds": MessageLookupByLibrary.simpleMessage(
            "Seleccione un punto dentro de Catalunya"),
        "formPublishAlert":
            MessageLookupByLibrary.simpleMessage("Publicar Alerta"),
        "formalizeComplaint":
            MessageLookupByLibrary.simpleMessage("Formalizar parada"),
        "insertText":
            MessageLookupByLibrary.simpleMessage("Introduzca el texto"),
        "language": MessageLookupByLibrary.simpleMessage("Idioma"),
        "networkErrorButton": MessageLookupByLibrary.simpleMessage(
            "Error de conexión, reintentar"),
        "ok": MessageLookupByLibrary.simpleMessage("Ok"),
        "select": MessageLookupByLibrary.simpleMessage("Selecciona"),
        "send": MessageLookupByLibrary.simpleMessage("Envía"),
        "showAlertsFrom":
            MessageLookupByLibrary.simpleMessage("Ver alertas des de: "),
        "update": MessageLookupByLibrary.simpleMessage("Actualiza")
      };
}
