/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:latlong2/latlong.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/models/panel.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/service/alerts.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'package:pareu_de_pararme_map/ui/widgets/alert_card.dart';
import 'package:pareu_de_pararme_map/ui/widgets/non_foss.dart';
import 'package:pareu_de_pararme_map/ui/widgets/marker.dart';
import 'package:provider/provider.dart';
import 'package:pareu_de_pararme_map/generated/l10n.dart';


class MainMap extends StatefulWidget {
  MainMap({Key? key, }) : super(key: key);

  @override
  _MainMap_State createState() => _MainMap_State();
}

class _MainMap_State extends State<MainMap> {
  // Dictionary marker alert to create the popups
  Map<Marker, Alert> markerAlertDictionary = <Marker, Alert>{};
  // Markers on the map
  List<Marker>  markers = [];

  // Init state of map
  LatLng barcelonaLatLng = LatLng(41.398231, 2.174603);
  double initialZoom = 13;

  // Controller for map plugin
  late final MapController _mapController;

  // Flags actions, disable rotation
  var interActiveFlags =
  InteractiveFlag.doubleTapZoom | InteractiveFlag.pinchZoom |
  InteractiveFlag.drag | InteractiveFlag.pinchMove | InteractiveFlag.flingAnimation;

  /// Used to store buttons like location button
  final nonRotatedLayers = <LayerOptions> [];

  /// Store the map plugins
  final mapPlugins = <MapPlugin>[];

  @override
  void initState() {
    super.initState();
    _mapController = MapController(); // Controller of flutter map
    nonRotatedLayers.addAll( getLocationButton(_mapController));
    mapPlugins.addAll(getMapPlugins());
  }

  /// Used to delete the new alert already positioned on the map when the panel is closed.
  deleteAddNewAlert (){
    context.read<NewAlertModel>( ).remove();
  }

  // From  https://stackoverflow.com/questions/61943711/google-maps-flutter-check-if-a-point-inside-a-polygon
  List<LatLng> _area = [
    // LatLng(42.88204791356731, 0.6963084519501355),
    // LatLng( 40.517278, 0.613961 ),
    // LatLng( 40.733333, 0.150000 ),
    // LatLng( 42.316667, 3.316667 ),
    // LatLng(  42.435712, 3.305209 ),
    // LatLng( 41.119802, 2.559890 ),
    LatLng(40.480120, 0.109520),
    LatLng(42.961448, 0.108490),
    LatLng(42.960443, 3.446960),
    LatLng(40.476203, 3.455200),
  ];

  bool _checkIfValidMarker(LatLng tap, List<LatLng> vertices) {
    int intersectCount = 0;
    for (int j = 0; j < vertices.length - 1; j++) {
      if (_rayCastIntersect(tap, vertices[j], vertices[j + 1])) {
        intersectCount++;
      }
    }
    return ((intersectCount % 2) == 1); // odd = inside, even = outside;
  }

  bool _rayCastIntersect(LatLng tap, LatLng vertA, LatLng vertB) {
    double aY = vertA.latitude;
    double bY = vertB.latitude;
    double aX = vertA.longitude;
    double bX = vertB.longitude;
    double pY = tap.latitude;
    double pX = tap.longitude;

    if ((aY > pY && bY > pY) || (aY < pY && bY < pY) || (aX < pX && bX < pX)) {
      return false; // a and b can't both be above or below pt.y, and a or
      // b must be east of pt.x
    }

    double m = (aY - bY) / (aX - bX); // Rise over run
    double bee = (-aX) * m + aY; // y = mx + b
    double x = (pY - bee) / m; // algebra is neat!

    return x > pX;
  }

  void _handleTap(LatLng latlng) {
    popupLayerController.hidePopup();
    if(context.read<PanelModel>().isOpen) {
      if(_checkIfValidMarker(latlng, _area)) {
        deleteAddNewAlert();
        context.read<NewAlertModel>()..add(latlng);
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).formPointOutsideBounds),
        ));
      }
    }
  }

  var popupAligment = AnchorAlign.top;

  @override
  Widget build(BuildContext context) {
    return Consumer3<NewAlertModel, AlertsListModel, PanelModel>(
      builder: (context, newAlertModel, alertsListModel, panelModel, child) {
        markers = [];
        // Add alerts markers received from service
        if(!panelModel.isOpen) {
          markers = alertsListModel.alerts.map((alert) {
            var marker = Marker(
              width: MAP_MARKER_SIZE,
              height: MAP_MARKER_SIZE,
              point: alert.location,
              anchorPos: AnchorPos.align(AnchorAlign.top),
              builder: (ctx) =>
                  Container(
                    child: AppMarker(
                      alert.alertType.icon,
                    ),
                  ),
            );
            markerAlertDictionary.addAll({marker:alert});
            return marker;
          }).toList();
        }
        // Add new alert marker to create new alert
        if(newAlertModel.newAlert != null && panelModel.isOpen) {
          var marker = Marker(
            width: MAP_MARKER_SIZE,
            height: MAP_MARKER_SIZE,
            anchorPos: AnchorPos.align(AnchorAlign.top),
            point: newAlertModel.newAlert!.location,
            builder: (ctx) => Container(
              child: AppMarker(
                newAlertModel.newAlert!.alertType.icon,
              ),
            ),
          );
          markerAlertDictionary.addAll({marker:newAlertModel.newAlert!});
          markers.add(marker);
        }
        return FlutterMap(
          mapController: _mapController,
          options: MapOptions(
            center: LatLng(barcelonaLatLng.latitude, barcelonaLatLng.longitude),
            zoom: initialZoom,
            interactiveFlags: interActiveFlags,
            onTap: _handleTap,
            plugins: mapPlugins,

          ),
          children: [
            TileLayerWidget(
              options: TileLayerOptions(
                urlTemplate:
                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
                // For example purposes. It is recommended to use
                // TileProvider with a caching and retry strategy, like
                // NetworkTileProvider or CachedNetworkTileProvider
                // tileProvider: NonCachingNetworkTileProvider(),
                tileProvider: NetworkTileProvider(),
              ),
            ),
            PopupMarkerLayerWidget(
              options: PopupMarkerLayerOptions(
                markers: markers,
                // popupSnap: widget.snap,
                popupController: popupLayerController,
                popupBuilder: (BuildContext context, Marker marker) =>
                    AlertCard(markerAlertDictionary[marker]!),
                markerRotate: false,
                popupAnimation: PopupAnimation.fade(duration: Duration(milliseconds: 200)),
              ),
            ),
          ],
          nonRotatedLayers: nonRotatedLayers,
          // nonRotatedLayers: <LayerOptions>[
          //   btn
          // ],
        );
      },
    );
  }
}
