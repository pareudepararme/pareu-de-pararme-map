/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/AppBuilder.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/models/alert_types.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/service/alert_types.dart';
import 'package:pareu_de_pararme_map/service/alerts.dart';
import 'package:pareu_de_pararme_map/service/base_api.dart' show BannedException;
import 'package:pareu_de_pararme_map/service/storage.dart';
import 'package:pareu_de_pararme_map/service/user_auth.dart' as authServie;
import 'package:pareu_de_pararme_map/service/user_auth.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'package:pareu_de_pararme_map/ui/widgets/captcha.dart';
import 'package:provider/provider.dart';
import 'package:pareu_de_pararme_map/generated/l10n.dart';


class Landing extends StatefulWidget {
  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> {
  String _jwt = "";
  String _user = "";
  String _pass = "";
  late UserAuth _userInfo;

  CaptchaController _captchaController = CaptchaController();

  bool _showCaptcha = false;
  bool _networkError = false;

  @override
  void initState() {
    super.initState();
    // Restore user prefered locale
    localeOrEmpty.then((value) {
      if (value.isNotEmpty) {
        S.load(Locale(value));
        AppBuilder.of(context)!.rebuild();
      }
    });
    _loadUserInfo().then((value) => _checkPath());
  }

  Future<void> _loadUserInfo() async {
    _jwt = await jwtOrEmpty;
    _user = await userOrEmpty;
    _pass = await passOrEmpty;
    _userInfo = _setUserInfo(_user, _pass, _jwt);
  }

  void _setNetworkError([setTo = true]) => setState(() => _networkError = setTo);
  void _setShowCaptcha([setTo = true]) => setState(() => _showCaptcha = setTo);
  UserAuth _setUserInfo(String user, String pass, String jwt) =>
      setUserInfo(context, user, pass, jwt);


  /// Check where is the next app path based on user data
  Future <void> _checkPath() async {
    try {
      print("Checking path");
      if (!_userInfo.jwtValidate()) {
        print("JWT not valid");
        // Try to login if pass and user is not empty
        if (_user.isNotEmpty && _pass.isNotEmpty) {
          try {
            await loginAndRedirect(context, _userInfo);
            return;
          }
          // After get a 401 exception from a bad login, lets try to signup again
          // for last time before understand is banned
          on BannedException {
            print("Maybe banned try to signup again");
          }
        }
        // Case login unsuccessful go to signup
        print("Try getting captcha");
        _captchaController.fromJson(await authServie.captcha());
        _setShowCaptcha();
    } else {
        print("JWT valid");
        pushMapScreen();
      }
    } catch (e) {
      _setNetworkError();
      throw e;
    }
  }

  Future<void> signup(CaptchaReq req) async {
    Map<String, dynamic> res = {};
    _setUserInfo("", "", "");
    try {
      res = await authServie.signup(CaptchaReq(req.captchaId, req.captchaResolution, req.phoneId));
      // res = await signup(req);
    } on BannedException {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).captchaError),
      ));
      await _checkPath();
      throw("You are banned");
    } catch(e){
      _setNetworkError();
      print("Error connecting to the server");
      throw(e);
    }
    if(res.isNotEmpty) {
      loginAndRedirect(context, _setUserInfo(res["username"], res["secret"], ""));
    }
  }

  /// Do login and store the jwt on the secret storage, then redirect to map screen
  Future<void> loginAndRedirect(BuildContext context, UserAuth _userInfo) async {
    var res = await authServie.login(_userInfo.user, _userInfo.pass);
    _userInfo = _setUserInfo(_userInfo.user, _userInfo.pass, res["access_token"]);
    pushMapScreen();
  }

  /// Go to map screen trying to get AlertsListModel and AlertTypesProvider
  /// before push it.
  Future<void> pushMapScreen() async{
    try {
      Provider.of<AlertsListModel>(context, listen: false).add(await getAlerts());
      Provider.of<AlertTypesProvider>(context, listen: false).add(await getAlertTypes());
    }
    // If JWT is well formed but not valid against the server go to login
    on BannedException {
      _setUserInfo(_user, _pass, "");
      _checkPath();
    }
    catch(e) {
      _setNetworkError();
      rethrow;
    }

    Navigator.pushNamedAndRemoveUntil(
        context, '/map', ModalRoute.withName('/map'));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage("assets/images/banner-home-gradient.png"), fit: BoxFit.cover),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if(_networkError)  Center(
                        child: ElevatedButton.icon(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(PRIMARY_BTN_COLOR)),
                          label: Text(S.of(context).networkErrorButton),
                          icon: Icon(Icons.refresh_outlined),
                          onPressed: () async {
                            _setNetworkError(false);
                            _checkPath();
                          },
                        ),
                      )
                      else if(_showCaptcha) CaptchaWidget(_captchaController, signup)
                      else  CircularProgressIndicator()
                    ],
                  ),
                ],
              ),
            )
        ),
      ),
    );
  }
}