/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/material.dart';

part 'alert_types.g.dart';

class AlertTypesProvider extends ChangeNotifier {
  List<AlertType> _alertTypes = [];
  List<AlertType> get alertTypes => _alertTypes;

  void add(List<AlertType> alertTypes) {
    _alertTypes = alertTypes;
    notifyListeners();
  }

  void remove() {
    _alertTypes = [];
    notifyListeners();
  }
}

/// Interface that define fow alert types have to be structured
@JsonSerializable(explicitToJson: true)
class AlertType {
  @JsonKey(name: "icon")
  final String base64Icon;
  @JsonKey(name: "alertDescription_es")
  final String alertDescriptionEs;
  @JsonKey(name: "alertTitle_es")
  final String alertTitleEs;
  @JsonKey(name: "alertDescription_cat")
  final String alertDescriptionCat;
  @JsonKey(name: "alertTitle_cat")
  final String alertTitleCat;
  @JsonKey()
  final String keyId;
  AlertType(this.keyId, this.base64Icon, this.alertDescriptionEs, this.alertTitleEs,
      this.alertDescriptionCat, this.alertTitleCat, ){
    this.icon = MemoryImage(base64.decode(this.base64Icon));
  }
  @JsonKey(ignore: true)
  late MemoryImage icon;

  @JsonKey(ignore: true)
  String get alertDescription => Intl.getCurrentLocale().contains("CA")
      ? this.alertDescriptionCat : this.alertDescriptionEs;

  @JsonKey(ignore: true)
  String get alertTitle  => Intl.getCurrentLocale().contains("CA")
      ? this.alertTitleCat : this.alertTitleEs;

  factory AlertType.fromJson(Map<String, dynamic> json) => _$AlertTypeFromJson(json);
  Map<String, dynamic> toJson() => _$AlertTypeToJson(this);
}

/// Alert types implementation
// final Map<String, AlertType> AlertTypes = {
//   "1" : MassiveNaziControl(),
//   "2" : SingleAcabAlert(),
// };
// class AlertTypesNames {
//   static const String MassiveNaziControl = "1";
//   static const String SingleAcabAlert = "2";
// }
// final List<AlertType> AlertTypesList = AlertTypes.values.toList();
//
// class SingleAcabAlert extends AlertType {
//   SingleAcabAlert( ) :
//         super(
//           "assets/images/Controles_guia_1.png",
//           "Single identifications, for example arbitrary documentation request on the street", // todo(intl)
//           "Identificació personal", // todo(intl)
//           "1",
//       );
// }
//
// class MassiveNaziControl extends AlertType {
//   MassiveNaziControl( ) :
//         super(
//           "assets/images/Controles_guia_3.png",
//           "Massive controls, for example: in front of a metro requesting documentations", // todo(intl)
//           "Redada policial", // todo(intl)
//           "2",
//       );
// }

class CreateAlert extends AlertType {
  CreateAlert( ) :
        super(
        "0",
        // "assets/images/Controles_guia_3.png",
        "iVBORw0KGgoAAAANSUhEUgAAARYAAAEWCAYAAACjTbhPAAAACXBIWXMAAA9gAAAPYAF6eEWNAAAQdUlEQVR4nO3d3XEbyxGG4T0u3+/JgHIEpC5xJZ4IREcgOgLRERjKgIrAUgQmIzB5hUuLEVjM4GwEdK3U8IEokAAX8832dL9Plcoqlw3h99uemZ7ZXx4eHjoAKOlPvJsASiNYABRHsAAojmABUBzBAqA4ggVAcQQLgOIIFgDFESwAiiNYABRHsAAojmABUBzBAqA4ggVAcQQLgOL+zFuaV9+vTre8+G3/3T6+dF33++b/bhgWN9nf46w46CmYvl+ddF33a9d1r+zP+PcTe5Xj349nesX3Xdd9tb+vQ+h3+zshFAzB0iCrNNbBcbIRHn2Al3dr/3mzETxfhmHx+47/HxwhWByz6uPEAmQdJkdJ345hHTL25ytVjl8EixN9v1pXH6f2n2+yvyd7urOgubHK5ksTzzo4gmUmFiSnG3+yViKlDRYy3/4QNPMgWCrp+9WvFiJnHUFS0zporixovvp5anERLEJWlZzZH4Y2PtxZyFxRzegQLIVthMn5jEu72M+9hcwllUxZBEshfb86t0B5G+IF5TNWMp/GPyxtH45gOYBVJxdWnUToIcF3ny1gWM6eiGCZwBrUlsybhHdnw6RP2d+IlyJYXsCGO+cESjrjXMySgNkfwbIHq1A+sUScHgGzJ4LlGQx58IQxYC6GYXHFG7QdwbKFNbNddl33zt2Tgyfjhslzlqp/xkFPj9g8yldCBXsYK9n/9v1qyZv1IyoWY1XKJ/pQMNGdVS/pu3k7KpbvbC7lK6GCA4xd1v/p+9XY15Re+orFyth/OHgqiOPaqpe0Hbxpg4UJWoiNQ6OzrBO7KYPFQuWGTYIQG49sOM0475JujoVQQUXj/rEbO2I0lVTBQqhgBinDJU2wECqYUbpwyVSxXBEqmNEYLld2gQsvRbD0/eqS/T5w4MgucOGFXxXq+9V4qtu/HDwVpfWB0V8e3er0dOOWIp4OorrdeL6bz/VVkoPGPwzDIvQ2gNDBYmXn16Cnu91unDy/cznT9kAtZ/zRDvbv7zz6cePc4NPA3dC/RT6hLnqwXAX7Yl5vnDA/qavTWs6XlcP2o51jMvU5n23c7SDKReJ+GBavHDwPibDBYvt//u3gqRzq4DB5zFYnbir9SP9W8mCkjZCJ0DEddkgUOVi+NLwKdL9xYrykJbxSuBQNlU02zD2zw8xb/ZzH4eGriHuKQgaLzSf808FTealbC5MqRx+Kq7pqV2N7HReNDntDVi1Rg6W1auXW5iCqT+bZUvz7wg97OwyL08KPuZNN+i4bGyaFrFrC9bHY1auVULm11YHTGVcIlvblLv2Y1Y3DxmFYjNXqX+zeQC3o7c4PoURskGvhQxrnUP46c6B8Y1fKkk1bdw5e0zpgfrPw9i7c4VARg+XMwXN4zoexac3ZCe8l53Tc3BpjDDgbkv1dUJWVdBRtH1GoYLGlSK99DuPBP6/HiTpv4+nCFYa7s0eGYXFpHcieq5dQw6FoFYvXauVzAwf+FPnRee0mteHRunrxqPpkt1K0YPH44Yy9HKnPP/XEqpfXDodGx5F2PocJFvtQPG1eG2zow+04nbHK8cSGp56EmWeJVLF4+lDSnnXaCutoPnUWLmGGQwRLeYRKI2x46ilcwmxKjBQsHsanhEpjnIULweKQh4rlglBpj4XLuYMJXYLFobkrlo9M1LbLLghz95KEOTmPezeXMbaxc8/exlk39Mfs70MJBEsZ4TaRJba0vVw4AMFyuI/Mq8Rh8y2hD7qugWA5zMCXMB6bK6NqOQDBcphLWvXD4oJxAILlMKwCBUXVchiCZbpr1UHXcIMLx0SRgqX2kCTFrTKTqx0s3jZFThYpWGqvzBAswVlFWnM4FGa+jqHQNHdM2qbBBWQCgmWasPfcxU9qVsJULA7V/LEzaZtHzWAJ02hJxTINnbZJ0FU9TaRgqVlFULFAgYrFm5o9JfSvpFPrtiHMsThFpyRaRsXiFJUEmhWphSFasDDRhlaF6brtqFgAN0J9d6lYAB9CfXepWCbo+1WY09SxlzcV3iYqFq9sGbjGLRwIFpRGxeIcwyE0J1qHL8EyjYe7LqKCSsPeUCtCHcEymacb0EOrRrCEq7IJFuB5NapTgsU7G6uqJ3BP23+nsKca1SnB0giqFpQir1iGYRHu4LCowaL+oGr0NcAHdcVSa+d0VQTLRDTJpaH+nENW1wyFpiNYcjgSv8qQ5yeHDBbbfq7uDWACN7i+X9X4jAmWxqg/MCqW+NSf8X3U28gQLNPRJBef+jMOexsZgmW6Y/HjY34Ey0Rhg6XGPEulMTjmow6WsHdZjH5fIfUHx3AoqL5fjZ9tL3x1oW/TGz1YmGfBVAyDDhA6WKxVWrlviKFQXOrPlmBpnPIDPKIDNyxpxTIMi7DzK12SYFF/gFQtwfT96lfxqt919PeQiuVwBEs8DIMOFD5Y7IBt5bIzwRKP+jMNPQzqklQsnfiDZJ4lHmWw3NnFLjSCpQyqliDsIqGcXwk/DOqyBIsdV3kv/CfOhI+NutQXiU8ZPs8sFUsnrlqoWOJQfpb30e4f9JRMwaK8UvTsGwpDWX2Gn7RdSxMsDIewS4X9QSnmV7pkFUvHcAg7nAvfoCF6t+2mbMGiHA4ds+zcPOXFIU2odNmCheEQnlJhmZlgCU75AStLaWgpLwqphkFd0mBhOIRtlBeFFL0rm9IFC8MhPFZhGESwJMFwCJuUF4M0TXGbsgbLpfCxGQ61R3kxSDW3spYyWCocpcBwqBEVhkHKi5hbWSuWTjzuZTjUDuVFIMURCdsQLBoMh9rBapBA2mCxe7oozx6lanGO1SCdzBVLx3AovQvhG3Ad+YZku6QOFuuGVPW0HNluWfilnF9JW6102YPFKJcDlVdEHMDOzzkSvYf32Vr4HyNYtMuBZ3aPGvhD74pQ+mCx5cBb0cP39LT4Y2Gv/FxS9q5sSh8shkncXM6EJ8XdZu1d2USwfK9aPglvHv+GnhZ3lHNfqSdt1wiWPyi/EEziOmErdarelcEuUukRLH9QjosZDvlBp20FBItRT+L2/Ypw8UH5OaSftF0jWH7EJG5gFu5M2lZAsGyw8bGqE5dJ3PkxaVsJwfIzJnEDEk/a3jNp+yOC5WfS4RCduLOhWqmIYHnExsmfRQ9PJ+4MKnTaEiyPECzbKb8oS+FjYzvlpO1nJm1/RrBsMQyLG+GZuEe2sxb1MAyqjGB5mrIngUncSvp+dSY8HuHOLkJ4hGB5gnj/0FuWnqtRhjgNcU8gWJ5H1dIwW2J+I3oFLDE/g2B5nnT/EEvPcsytzIRgeYYdhqxceqZqEbGh5jvRww8Mg55HsOymXB5m/5CO9OjJzCfw74Ng2UG86/mIXc/l2RBTWQ3Si7QDwbIf5ReJL2l5yoa4axridiNY9kDDXHNYYp4ZwbI/5ReKqqUQG1qqGuJuaYjbD8GypwpntVC1lKEMaZaY90SwvAwrRI5VuLshwbInguVlroRt/u9o8z8Yk+xOECwvYL0LzLU4ZNUK7ftOECwvd0nV4hJzK44QLC9E1eKPuFqhfX8CgmUa5RWMquXlpPcKon3/5QiWCcTn4nZsTtwfmw19Ilimky49c6TC3pSfA9XKRATLRBVO86dq2UFcrXRM2k5HsBxGebW8oGrZSfn+c/r+AQiWA1C1zKdCtcLq3AEIlsNRtcyDasUxguVAVC31Ua34R7CUQdVSF9WKcwRLARWqFnopDNVKGwiWcpRfSLpx/6AMWaqVQgiWQip046a/ktqeoLfCfyL9e1wKwVIWVYsWcyuNIFgKomrREe9g7qhWyiJYyqNq0aBaaQjBUliFqiXdChHVSnsIFg3lF/VtwhP9WQlqDMEiwFxLOXafoGPRww9UKxoEi47yC5vpPkTq81aoVgQIFhHmWg4nvqshp8MJESxaS+GJ/sf2wwvJ9kcpf/icDidEsAhZ1cKJ/tNc2D4pBaoVMYJFT3kfoqO+X4U7VsGqFeXroloRI1jEatyHKOCxCpfCauWeakWPYKlDWbWEOgyqxrEIVCt6BEsF9kVWHwYVpdVfWU1wD+ZKCJZKhmFxaWW4Qh9hIpdjEeIgWOpSb1A88fiiX0D5/txSrdRDsFRkX+w74b/Y7KSk9eSw0TAIgqU+5UTr2Op/5vWFP8VWtdTVyo3w8fEIwVKZfcFvhf9qi1XLhbB1v+MWKvURLPNQXp2bapqr0Aw3HovwRfj42IJgmYFVLdJjFRpqmlM2w3XMrcyDYJmP8gvfxPKzrWIpm+E+cCzCPAiWmdgX/oPwX3/fQNOccj6IjYYzIljmpWz1H7nt27DVK+nyMq378yFYZlRhg6LLk+YqnLVyb53OmAnBMrNhWCyFrf6d06qF5eXgCBYf0iw/27yP8vmMzXBXwsfHHggWB6zVX9k052n5meXlBAgWP9TLz7PPOVTYvfyZ1n0fCBYnKjTNvXMwkateXqZacYJg8UX9w5jth2fzPKobj3XcI8gXgsWRCk1zb+a4ZUiF3cucY+sMweKPumnucoaJXPmELc1wvhAsztgPRLkcW3Ufkc3rKPcDcTKcQwSLQxVOmntf8RhL9RCFCVuHCBa/1E1t8jmJChO2LC87RbA4ZT+Ya+Gzk07kVpiwZXnZMYLFt4uGJ3LVE7YsLztGsDhW4abykoncChO2LC87R7D4p7zRWSeayFX/6C9YXvaNYHGuwvJzVzII+n61FE/Ysnu5Ab88PDxkfw+a0PerG/GJa6UM4rmV15y67x8VSztaObxIGSofCZU2ECyNsB/Ux8xvAcvL7SBY2rIULz97xoRtQwiWhtgPK+NVm/1AjSFYGmOnzyv3EXnE4diNIVjalOmHxr2XG0SwNKjCMZZeDFQrbSJY2qXeR+QBBzg1imBpVIKJ3FvuZtgugqVhwSdyGQI1jGBpX8QfIB22jSNYGhdwIpcO2wAIlhgiTeTSYRsAwRJApaMVaqDDNgiCJYgKN5avofrN1KBBsMTSctXygTNs4yBYAmn4aAXOsA2GYIlnKT4jV+GcCdtYCJZgGpzIveamY/EQLAHZYdPKm52VwibDoAiWuFrobVkyYRsTwRJUhZudHeqOTYZxESyBDcNi6XiTIj0rgREs8Xmcw/jAJsPYCJbgbMXFU28LPSsJECw5eOptoWclAYIlAUe9LfSsJEGwJOGgt2VgwjYPgiWXOXtbOGclEYIlEettmeN0Ns5ZSYZgScaa0mqe28IQKCGCJaeaP3Ta9hMiWBKyH/qHCq+cewMl9cvDw0P29yCtvl+N3a/Hwtf/mg7bnKhYclMOiWjbT4yKJbm+X41DlfeF34Vx5/JJ9vc2MyoWKNr9WQVKjmBJzprWSgYBQyAwFMJ3hYZEDIHwDRUL1koMiTi/Ft8QLPimwJDoIzuXsUaw4P8OOBTqfqY9SHCKYMFjU4ZEHN6EHxAs+MGEQ6EYAuEnBAt+YodCfd7jnbljCIRtCBY85WKPIRFDIGxFsGCrPVaJaITDk2iQw7OeaJyjEQ7PomLBLttWidgLhGcRLHjWliERQyDsxFAIe7Eh0ckwLE55x7DLn3mHsCeWlbE3KhYAxTHHAqA4ggVAcQQLgOIIFgDFESwAiiNYABRHsAAojmABUBzBAqA4ggVAcQQLgOIIFgDFESwAiiNYABRHsAAoq+u6/wHezR4+n84wDgAAAABJRU5ErkJggg==",
        "", // description ES
        "Crea una alerta", // Title ES
        "", //  description CAT
        "Crea una alerta", // Title CAT
      );
}

