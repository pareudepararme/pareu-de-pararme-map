

/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:json_annotation/json_annotation.dart';

import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:pareu_de_pararme_map/models/alert_types.dart';

part 'alert.g.dart';

/// Used for the app state
class NewAlertModel extends ChangeNotifier {
  /// Internal, private state of the cart.
  Alert? _newAlert;

  /// An unmodifiable view of the items in the cart.
  Alert? get newAlert => _newAlert;

  /// Adds [item] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  void add(LatLng item) {
    _newAlert = Alert(
      lat: item.latitude,
      lon: item.longitude,
      alertType: CreateAlert(),
      description: "Crea una alerta ", // todo(intl)
      time: DateTime.now().millisecondsSinceEpoch
    );
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  /// Removes all items from the cart.
  void remove() {
    _newAlert = null;
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
}

/// Used for the app state
class AlertsListModel extends ChangeNotifier {
  /// Internal, private state of the cart.
  List<Alert> _alerts = <Alert>[];

  /// An unmodifiable view of the items in the cart.
  List<Alert> get alerts => _alerts;

  void add(List<Alert> alerts) {
    _alerts = alerts;
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  /// Removes all items from the cart.
  void remove() {
    _alerts = <Alert>[];
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
}

/// Alert class
///
/// This class will represent an alert
@JsonSerializable(explicitToJson: true)
class Alert {
  @JsonKey(name: "user_created_at")
  int time;
  String description;
  // int? confirmations; // Unused
  // int? abuses; // Unused
  // Address? adress; // Unused
  @JsonKey(name: "alert_type")
  AlertType alertType;

  late double lat;
  late double lon;

  @JsonKey(ignore: true)
  LatLng get location => LatLng(lat, lon);

  Alert({
    required this.time,
    required this.description,
    required this.lat,
    required this.lon,
    required this.alertType
  });

  factory Alert.fromJson(Map<String, dynamic> json) => _$AlertFromJson(json);
  Map<String, dynamic> toJson() =>
    (_$AlertToJson(this))
      ..["alert_type_id"] = this.alertType.keyId
      ..["alert_type"] = ""
      ..["lat"] = this.lat.toString()
      ..["lon"] = this.lon.toString();

}